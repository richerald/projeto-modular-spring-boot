package core.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import core.domain.Pessoa;
import core.persistence.PessoaRepository;

import java.util.List;

@RestController
public class PessoaController {

    @Autowired
    public PessoaRepository PessoaRepository;

    @RequestMapping(value = "/pessoas", method = RequestMethod.GET)
    public List<Pessoa> findAll(){
        List<Pessoa> pessoas = PessoaRepository.findAll();
        return pessoas;
    }
}
