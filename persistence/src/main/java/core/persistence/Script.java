package core.persistence;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import core.domain.Pessoa;

import java.util.Arrays;

@Component
public class Script implements CommandLineRunner {

    @Autowired
    private PessoaRepository PessoaRepository;

    @Override
    public void run(String... strings) throws Exception {
        Pessoa Joao = new Pessoa("Joao",24,true);
        Pessoa Eduardo = new Pessoa("Eduardo",25,true);
        Pessoa Carlos = new Pessoa("Carlos",26,true);

        PessoaRepository.save(Arrays.asList(Joao,Eduardo,Carlos));

    }
}
